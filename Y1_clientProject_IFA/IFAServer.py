# IMPORTING OS MODULE - DO NOT DELETE REQUIRED FOR SERVER TO RUN!
import os
# IMPORTING METHODS FROM FLASK MODULE - DO NOT DELETE REQUIRED FOR SERVER TO RUN!
from flask import Flask, redirect, request, render_template, make_response, escape, session
# IMPORTING SQLITE MODULE FOR DATABSE USe
import sqlite3
# DO NOT DELETE REQUIRED FOR SERVER TO RUN!

DATABASE = 'IFADatabase.db'

app = Flask(__name__)

# WILL ALLOW THESE TYPES OF FILES TO RUN ON THE SERVER.
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# DO NOT DELETE REQUIRED FOR SERVER TO RUN!
app = Flask(__name__)
#===============================================================================
#======================LOGIN SERVER FUNCTIONS/ROUTES============================
@app.route("/Login", methods = ['GET','POST'])
def login():
    if request.method=='POST':
        uName = request.form.get('username', default="Error")
        pw = request.form.get('password', default="Error")
        if checkCredentials(uName, pw):
            resp = make_response(render_template('Home.html', msg='hello '+uName, username = uName))
            resp.set_cookie('username', uName)
        else:
            resp = make_response(render_template('Home.html', msg='Incorrect  login',username='Guest'))
        return resp
    else:
        username = request.cookies.get('username')
        return render_template('Login.html', msg='', username = username)

def checkCredentials(uName, pw):
	pw = "123"
	return pw

#===============================================================================
#=====================ADMIN PANEL SERVER FUNCTIONS/ROUTES=======================

@app.route("/Admin")    #Declaring route /Admin
def admin():            #When route is accessed define this function
    return render_template('adminPanel.html', msg = '') #Return adminpannel page

@app.route("/Admin/reCreateDB")
def reCreate():
    createDB()
    return render_template('adminPanel.html', msg = 'DB recreated')

@app.route("/Admin/DeleteDB")
def deleteTables():
    deleteTables()
    return render_template('adminPanel.html', msg = 'DB Deleted')

@app.route("/Admin/ListAll")
def list():
    msg = getData()
    return render_template('adminPanel.html', msg = msg)

def createDB():
    conn = sqlite3.connect(DATABASE)    #Connecting to the IFADatabase and creatingthese tables with these fields and parameters.
    conn.execute('CREATE TABLE IF NOT EXISTS `Health` (\
                            `healthID`INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
                            `healthStatus`TEXT NOT NULL,\
                            `smokerStatus`TEXT NOT NULL,\
                            `cigsADay`INTEGER NOT NULL,\
                            `drinkerStatus` TEXT NOT NULL,\
                            `unitsAWeek` INTEGER NOT NULL,\
                            `height` REAL NOT NULL,\
                            `weight` REAL NOT NULL,\
                            `medicalConditions` TEXT,\
                            `hazardousPursuits` TEXT );')
    conn.execute('CREATE TABLE IF NOT EXISTS "Dependants" (\
                            "dependantID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
							"dependantName" TEXT NOT NULL,\
							"dependantAge" INTEGER NOT NULL,\
							"dependantGender" TEXT NOT NULL,\
							"dependantDisability" TEXT );')
    conn.execute('CREATE TABLE IF NOT EXISTS "Clients" (\
                            `clientID`INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
							"title" TEXT,\
							"forename" TEXT NOT NULL,\
							"initial" TEXT, \
							"surname" TEXT NOT NULL,\
							"preferedName" TEXT,\
							"age" INTEGER NOT NULL,\
							"sex" TEXT NOT NULL,\
							"dob" TEXT NOT NULL,\
							"maritalStatus" TEXT,\
							"maidenName" TEXT,\
							"retire" TEXT,\
							"address" TEXT NOT NULL,\
							"telephone" INTEGER NOT NULL,\
							"hours" TEXT,\
							"email" TEXT NOT NULL,\
							"ethics" TEXT,\
							"changes" TEXT);')
    conn.execute('CREATE TABLE IF NOT EXISTS "Income" (\
                            `incomeID`INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
							"employment" TEXT NOT NULL,\
							"selfEmployment" TEXT NOT NULL,\
							"pensions" TEXT, \
							"interest" TEXT NOT NULL,\
							"investment" TEXT,\
							"rental" INTEGER NOT NULL,\
							"capital" TEXT NOT NULL,\
							"maintenance" TEXT NOT NULL,\
							"insurance" TEXT NOT NULL,\
							"benefit" TEXT);')
    conn.execute('CREATE TABLE IF NOT EXISTS "Expenditure" (\
                            "expenditureID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
                            "clientProperty" REAL,\
                            "housekeeping" REAL,\
                            "transport" REAL,\
                            "dependents" REAL,\
                            "pets" REAL,\
                            "profFees" REAL,\
                            "borrowCosts" REAL,\
                            "savings" REAL,\
                            "protectionPolicies" REAL,\
                            "other" REAL,\
                            "spendings" REAL);')
    conn.execute('CREATE TABLE IF NOT EXISTS "Assets" (\
                            "assetsID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
                            "pensions" REAL,\
                            "investments" REAL,\
                            "rentalProperties" REAL,\
                            "vehicles" REAL,\
                            "personalEffects" REAL,\
                            "businessInterests" REAL);')
    conn.execute('CREATE TABLE IF NOT EXISTS "ClientRecords" (\
                            "recordID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
                            "clientID" INTEGER NOT NULL UNIQUE,\
                            "healthID" INTEGER NOT NULL UNIQUE,\
                            "dependantID" INTEGER UNIQUE,\
                            "incomeID" INTEGER NOT NULL UNIQUE,\
                            "expenditureID" INTEGER NOT NULL UNIQUE,\
                            "assetsID" INTEGER NOT NULL,\
                            FOREIGN KEY(clientID) REFERENCES Clients(cleintID),\
                            FOREIGN KEY(healthID) REFERENCES Health(healthID),\
                            FOREIGN KEY(dependantID) REFERENCES Dependants(dependantID),\
                            FOREIGN KEY(incomeID) REFERENCES Income(incomeID),\
                            FOREIGN KEY(expenditureID) REFERENCES Expenditure(expenditureID),\
                            FOREIGN KEY(assetsID) REFERENCES Assets(assetsID));')
    conn.close() # Close the connection to the DB


def getData():  #Defining the getData Function.
    msg = []    #Craeting empty list called msg
    conn = sqlite3.connect(DATABASE) #connecting to DB
    c = conn.cursor()   #Creating a cursor
    c.execute('SELECT * FROM Clients')  #Selecting all data from all these different tables and appending them to the list
    msg.append( c.fetchall()  )
    c.execute('SELECT * FROM Health')
    msg.append( c.fetchall()  )
    c.execute('SELECT * FROM Dependants')
    msg.append(  c.fetchall()  )
    c.execute('SELECT * FROM Income')
    msg.append(  c.fetchall()  )
    c.execute('SELECT * FROM Expenditure')
    msg.append(  c.fetchall()  )
    c.execute('SELECT * FROM Assets')
    msg.append(  c.fetchall()  )
    c.execute('SELECT * FROM ClientRecords')
    msg.append(  c.fetchall()  )
    print(msg)                      #printing list in server console
    return msg

def deleteTables():     #defining a new function deleteTables
    conn = sqlite3.connect(DATABASE)    #connecting to the DB
    conn.execute('DROP TABLE IF EXISTS Health')     # Once connected to the DB drop all the tables.
    conn.execute('DROP TABLE IF EXISTS Clients')
    conn.execute('DROP TABLE IF EXISTS Dependants')
    conn.execute('DROP TABLE IF EXISTS Income')
    conn.execute('DROP TABLE IF EXISTS personalDetails')
    conn.execute('DROP TABLE IF EXISTS Expenditure')
    conn.execute('DROP TABLE IF EXISTS Assets')
    conn.execute('DROP TABLE IF EXISTS ClientRecords')
    conn.close()

#===============================================================================
#========================HOME SERVER FUNCTIONS/ROUTES===========================

@app.route("/HealthDisplay", methods=['GET'])
def returnLogin():
    if request.method == 'GET':
        print("Processing Request")
        conn = sqlite3.connect('IFADatabase.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM Health")
        data = cur.fetchall()
        return render_template('Health.html', data=data)

@app.route("/ClientsDisplay", methods=['GET'])
def returnClients():
    if request.method == 'GET':
        print("Processing Request")
        conn = sqlite3.connect('IFADatabase.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM Clients")
        data = cur.fetchall()
        return render_template('Client.html', data=data)

@app.route("/DependantsDisplay", methods=['GET'])
def returnDependants():
    if request.method == 'GET':
        print("Processing Request")
        conn = sqlite3.connect('IFADatabase.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM Dependants")
        data = cur.fetchall()
        return render_template('Dependant.html', data=data)

@app.route("/IncomeDisplay", methods=['GET'])
def returnIncome():
    if request.method == 'GET':
        print("Processing Request")
        conn = sqlite3.connect('IFADatabase.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM Income")
        data = cur.fetchall()
        return render_template('income.html', data=data)

@app.route("/AssetsDisplay", methods=['GET'])
def returnAssets():
    if request.method == 'GET':
        print("Processing Request")
        conn = sqlite3.connect('IFADatabase.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM Assets")
        data = cur.fetchall()
        return render_template('assets.html', data=data)

@app.route("/ExpenditureDisplay", methods=['GET'])
def returnExpenditure():
    if request.method == 'GET':
        print("Processing Request")
        conn = sqlite3.connect('IFADatabase.db')
        cur = conn.cursor()
        cur.execute("SELECT * FROM Expenditure")
        data = cur.fetchall()
        return render_template('expenditure.html', data=data)

@app.route("/People")
def people():
    return render_template('people.html', msg = 'Add Health Details')

#===============================================================================
#================CLIENT PERSONAL DETAILS SERVER FUNCTIONS/ROUTES================

@app.route("/People/personalDetails")
def personalDetails():
    return render_template('personalDetails.html', msg = '')

@app.route("/People/addPersonalDetails", methods = ['GET','POST'])
def addPersonalDetails():
	if request.method == 'POST':
		title = request.form.get('title')
		forename = request.form.get('forename')
		initial = request.form.get('initial')
		surname = request.form.get('surname')
		preferedName = request.form.get('preferedName')
		age = request.form.get('age')
		sex = request.form.get('sex')
		dob = request.form.get('dob')
		maritalStatus = request.form.get('maritalStatus')
		maidenName = request.form.get('maidenName')
		retire = request.form.get('retire')
		address = request.form.get('address')
		telephone = request.form.get('telephone')
		hours = request.form.get('hours')
		email = request.form.get('email')
		ethics = request.form.get('ethics')
		changes = request.form.get('changes')
		conn = sqlite3.connect(DATABASE)
		conn.execute("INSERT INTO `Clients`(`title`,`forename`,'initial','surname','preferedName','age','sex','dob','maritalStatus','maidenName','retire','address','telephone','hours','email','ethics','changes') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(title,forename,initial,surname,preferedName,age,sex,dob,maritalStatus,maidenName,retire,address,telephone,hours,email,ethics,changes))
		conn.commit()
	conn.close()
	msg = forename + " " + surname  + "'s details have been added"
	return render_template('personalDetails.html', msg = msg)

#===============================================================================
#============CLIENT HEALTH DETAILS SERVER FUNCTIONS/ROUTES======================

@app.route("/People/Health")
def peopleHealthForm():
    return render_template('peopleHealth.html', msg = 'Add Health Details')

@app.route("/HealthDisplay")
def health_display():
    return render_template('Health.html', msg = 'health')

@app.route("/People/addClientHealth", methods = ['POST'])
def peopleAddHealth():
    healthStatus = request.form.get('healthStatus', default="Error")#rem: args for get form for post
    smokerStatus = request.form.get('smokerStatus', default="Error")
    cigsADay = request.form.get('cigsADay', default="Error")
    drinkerStatus = request.form.get('drinkerStatus', default="Error")
    unitsAWeek = request.form.get('unitsAWeek', default="Error")
    height = request.form.get('height', default="Error")
    weight = request.form.get('weight', default="Error")
    medicalConditions = request.form.get('medicalConditions', default="Error")
    hazardousPursuits = request.form.get('hazardousPursuits', default="Error")
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO Health ('healthStatus', 'smokerStatus', 'cigsADay', 'drinkerStatus', 'unitsAWeek', 'height', 'weight','medicalConditions', 'hazardousPursuits')\
                     VALUES (?,?,?,?,?,?,?,?,?)",(healthStatus, smokerStatus, cigsADay, drinkerStatus, unitsAWeek, height, weight, medicalConditions, hazardousPursuits) )
        conn.commit()
        msg = "Record successfully added"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

#===============================================================================
#===============CLIENT DEPENDENCIES SERVER FUNCTIONS/ROUTES=====================

@app.route("/People/Dependants")
def peopleDependants():
	return render_template('Dependencies.html', msg = 'Add Dependencies')

@app.route("/People/addDependencies", methods = ['POST'])
def peopleAddDependencies():
	dependantName = request.form.get('dependantName', default = 'Error')
	dependantAge = request.form.get('dependantAge', default = 'Error')
	dependantGender = request.form.get('dependantGender', default = 'Error')
	dependantDisability = request.form.get('dependantDisability', default = 'Error')
	print(dependantName)
	try:
		conn = sqlite3.connect(DATABASE)
		cur = conn.cursor()
		cur.execute("INSERT INTO Dependants ('dependantName', 'dependantAge', 'dependantGender', 'dependantDisability')\
					 VALUES (?,?,?,?)",(dependantName, dependantAge, dependantGender, dependantDisability) )
		conn.commit()
		msg = "Record successfully added"
	except Exception as e:
		conn.rollback()
		print(str(e))
		msg = "error in insert operation"
	finally:
		return msg
		conn.close()
#===============================================================================
#=======================SEARCH FORM SERVER FUNCTIONS/ROUTES=====================
@app.route("/Index")
def Index():
    return render_template('index.html', msg = '')

@app.route("/Search")
def SearchPage():
    return render_template('search.html', msg = 'Add Health Details')

@app.route("/Search/surname",methods = ['GET','POST'])
def surnameSearch():
	if request.method == 'POST':
		surname = request.form.get('surname')
		clientID = request.form.get('clientID')
		conn = sqlite3.connect(DATABASE)
		cur = conn.cursor()
		if surname=="":
			cur.execute("SELECT * FROM personalDetails WHERE clientID =?;", [clientID])
		elif str(clientID)== "":
			cur.execute("SELECT * FROM personalDetails WHERE surname =?;", [surname])
		else:
			cur.execute("SELECT * FROM personalDetails WHERE surname=? AND clientID =?;", [surname, clientID])
		data = cur.fetchall()
	conn.close()
	return render_template('search.html', data = data)

#===============================================================================
#============FINANCE INCOME DETAILS SERVER FUNCTIONS/ROUTES=====================

@app.route("/Finance/Income")
def financeIncome():
    return render_template('income.html', msg = '')

@app.route("/Finance/addIncome", methods = ['GET','POST'])
def addIncome():
	if request.method == 'POST':
		employment = request.form.get('employment')
		selfEmployment = request.form.get('selfEmployment')
		pensions = request.form.get('pensions')
		interest = request.form.get('interest')
		investment = request.form.get('investment')
		rental = request.form.get('rental')
		capital = request.form.get('capital')
		maintenance = request.form.get('maintenance')
		insurance = request.form.get('insurance')
		benefit = request.form.get('benefit')

		conn = sqlite3.connect(DATABASE)
		conn.execute("INSERT INTO `Income`(`employment`,`selfEmployment`,'pensions','interest','investment','rental','capital','maintenance','insurance','benefit') VALUES (?,?,?,?,?,?,?,?,?,?)",(employment,selfEmployment,pensions,interest,investment,rental,capital,maintenance,insurance,benefit))
		conn.commit()
	conn.close()
	return render_template('income.html', msg = '')


#===============================================================================
#============EXPENDITURE INCOME DETAILS SERVER FUNCTIONS/ROUTES=================

@app.route("/Finance/Expenditure")
def financeExpenditure():
    return render_template('expenditureTest.html', msg = '')

@app.route("/Finance/addExpenditure", methods = ['POST'])
def addExpenditure():
    clientProperty = request.form.get('clientProperty', default = 'Error')
    housekeeping = request.form.get('housekeeping', default = 'Error')
    transport = request.form.get('transport', default = 'Error')
    dependents = request.form.get('dependents', default = 'Error')
    pets = request.form.get('pets', default = 'Error')
    profFees = request.form.get('profFees', default = 'Error')
    borrowCosts = request.form.get('borrowCosts', default = 'Error')
    savings = request.form.get('savings', default = 'Error')
    protectionPolicies = request.form.get('protectionPolicies', default = 'Error')
    other = request.form.get('other', default = 'Errors')
    spendings = request.form.get('spendings', default = 'Error')
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO Expenditure ('clientProperty', 'housekeeping', 'transport', 'dependents', 'pets', 'profFees', 'borrowCosts', 'savings', 'protectionPolicies', 'other', 'spendings')\
                     VALUES (?,?,?,?,?,?,?,?,?,?,?)",(clientProperty, housekeeping, transport, dependents, pets, profFees, borrowCosts, savings, protectionPolicies, other, spendings) )
        conn.commit()
        msg = "Record successfully added"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

#===============================================================================
#=================ASSETS INCOME DETAILS SERVER FUNCTIONS/ROUTES=================

@app.route("/Finance/Assetss")
def financeAssets():
    return render_template('assets.html', msg = '')

@app.route("/Finace/addAssets")
def addAssets():
    pensions = request.form.get('pensions', default = 'Error')
    savings = request.form.get('savings', default = 'Error')
    investments = request.form.get('investments', default = 'Error')
    rentalProperties = request.form.get('rentalProperties', default = 'Error')
    vehicles = request.form.get('vehicles', default = 'Error')
    personalEffects = request.form.get('personalEffects', default = 'Error')
    businessInterests = request.form.get('businessInterests', default = 'Error')
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO Assets ('pensions', 'savings', 'investments', 'rentalProperties', 'vehicles', 'personalEffects', 'businessInterests')\
                     VALUES (?,?,?,?,?,?,?)",(pensions, savings, investments, rentalProperties, vehicles, personalEffects, businessInterests) )
        conn.commit()
        msg = "Record successfully added"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

#===============================================================================
#============SIGNUP ROUTES AND SERVER FUNCTIONS=================================

@app.route("/SignupClient")
def newClient():
    return render_template('signupClient.html', msg = '')

@app.route("/Signup/addSignupClient", methods = ['GET','POST'])
def addSignupClient():
	if request.method == 'POST':
		title = request.form.get('title')
		forename = request.form.get('forename')
		initial = request.form.get('initial')
		surname = request.form.get('surname')
		preferedName = request.form.get('preferedName')
		age = request.form.get('age')
		sex = request.form.get('sex')
		dob = request.form.get('dob')
		maritalStatus = request.form.get('maritalStatus')
		maidenName = request.form.get('maidenName')
		retire = request.form.get('retire')
		address = request.form.get('address')
		telephone = request.form.get('telephone')
		hours = request.form.get('hours')
		email = request.form.get('email')
		ethics = request.form.get('ethics')
		changes = request.form.get('changes')
		conn = sqlite3.connect(DATABASE)
		conn.execute("INSERT INTO `Clients`(`title`,`forename`,'initial','surname','preferedName','age','sex','dob','maritalStatus','maidenName','retire','address','telephone','hours','email','ethics','changes') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(title,forename,initial,surname,preferedName,age,sex,dob,maritalStatus,maidenName,retire,address,telephone,hours,email,ethics,changes))
		conn.commit()
	conn.close()
	msg = forename + " " + surname  + "'s details have been added"
	return render_template('signupClient.html', msg = msg)

@app.route("/SignupHealth")
def newHealth():
    return render_template('signupHealth.html', msg = '')

@app.route("/Signup/addSignupHealth", methods = ['POST'])
def addSignupHealth():
    healthStatus = request.form.get('healthStatus', default="Error")#rem: args for get form for post
    smokerStatus = request.form.get('smokerStatus', default="Error")
    cigsADay = request.form.get('cigsADay', default="Error")
    drinkerStatus = request.form.get('drinkerStatus', default="Error")
    unitsAWeek = request.form.get('unitsAWeek', default="Error")
    height = request.form.get('height', default="Error")
    weight = request.form.get('weight', default="Error")
    medicalConditions = request.form.get('medicalConditions', default="Error")
    hazardousPursuits = request.form.get('hazardousPursuits', default="Error")
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO Health ('healthStatus', 'smokerStatus', 'cigsADay', 'drinkerStatus', 'unitsAWeek', 'height', 'weight','medicalConditions', 'hazardousPursuits')\
                     VALUES (?,?,?,?,?,?,?,?,?)",(healthStatus, smokerStatus, cigsADay, drinkerStatus, unitsAWeek, height, weight, medicalConditions, hazardousPursuits) )
        conn.commit()
        msg = "Record successfully added"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

@app.route("/NewDependant")
def newDependant():
    return render_template('signupDependant.html', msg = '')

@app.route("/Signup/addSignupDependant", methods = ['POST'])
def signupAddDependants():
	dependantName = request.form.get('dependantName', default = 'Error')
	dependantAge = request.form.get('dependantAge', default = 'Error')
	dependantGender = request.form.get('dependantGender', default = 'Error')
	dependantDisability = request.form.get('dependantDisability', default = 'Error')
	print(dependantName)
	try:
		conn = sqlite3.connect(DATABASE)
		cur = conn.cursor()
		cur.execute("INSERT INTO Dependants ('dependantName', 'dependantAge', 'dependantGender', 'dependantDisability')\
					 VALUES (?,?,?,?)",(dependantName, dependantAge, dependantGender, dependantDisability) )
		conn.commit()
		msg = "Record successfully added"
	except Exception as e:
		conn.rollback()
		print(str(e))
		msg = "error in insert operation"
	finally:
		return msg
		conn.close()

@app.route("/NewIncome")
def newIncome():
    return render_template('signupIncome.html', msg = '')

@app.route("/Signup/addSignupIncome", methods = ['GET','POST'])
def addSignupIncome():
    if request.method == 'POST':
        employment = request.form.get('employment')
        selfEmployment = request.form.get('selfEmployment')
        pensions = request.form.get('pensions')
        interest = request.form.get('interest')
        investment = request.form.get('investment')
        rental = request.form.get('rental')
        capital = request.form.get('capital')
        maintenance = request.form.get('maintenance')
        insurance = request.form.get('insurance')
        benefit = request.form.get('benefit')

        conn = sqlite3.connect(DATABASE)
        conn.execute("INSERT INTO `Income`(`employment`,`selfEmployment`,'pensions','interest','investment','rental','capital','maintenance','insurance','benefit') VALUES (?,?,?,?,?,?,?,?,?,?)",(employment,selfEmployment,pensions,interest,investment,rental,capital,maintenance,insurance,benefit))
        conn.commit()
    conn.close()
    msg = "Record added"
    return render_template('signupIncome.html', msg = '')

@app.route("/NewExpenditure")
def newExpenditure():
    return render_template('signupExpenditure.html', msg = '')

@app.route("/Signup/addSignupExpenditure", methods = ['POST'])
def addSignupExpenditure():
    clientProperty = request.form.get('clientProperty', default = 'Error')
    housekeeping = request.form.get('housekeeping', default = 'Error')
    transport = request.form.get('transport', default = 'Error')
    dependents = request.form.get('dependents', default = 'Error')
    pets = request.form.get('pets', default = 'Error')
    profFees = request.form.get('profFees', default = 'Error')
    borrowCosts = request.form.get('borrowCosts', default = 'Error')
    savings = request.form.get('savings', default = 'Error')
    protectionPolicies = request.form.get('protectionPolicies', default = 'Error')
    other = request.form.get('other', default = 'Errors')
    spendings = request.form.get('spendings', default = 'Error')
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO Expenditure ('clientProperty', 'housekeeping', 'transport', 'dependents', 'pets', 'profFees', 'borrowCosts', 'savings', 'protectionPolicies', 'other', 'spendings')\
                     VALUES (?,?,?,?,?,?,?,?,?,?,?)",(clientProperty, housekeeping, transport, dependents, pets, profFees, borrowCosts, savings, protectionPolicies, other, spendings) )
        conn.commit()
        msg = "Record successfully added"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

@app.route("/NewAssets")
def newAssets():
    return render_template('signupAssets.html', msg = '')

@app.route("/Signup/addSignupAssets", methods = ['POST'])
def addSignupAssets():
    pensions = request.form.get('pensions', default = 'Error')
    investments = request.form.get('investments', default = 'Error')
    rentalProperties = request.form.get('rentalProperties', default = 'Error')
    vehicles = request.form.get('vehicles', default = 'Error')
    personalEffects = request.form.get('personalEffects', default = 'Error')
    businessInterests = request.form.get('businessInterests', default = 'Error')
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO Assets ('pensions', 'investments', 'rentalProperties', 'vehicles', 'personalEffects', 'businessInterests')\
                     VALUES (?,?,?,?,?,?)",(pensions, investments, rentalProperties, vehicles, personalEffects, businessInterests) )
        conn.commit()
        msg = "Record successfully added"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

@app.route("/NewRecord")
def newRecord():
    return render_template('signupRecord.html', msg = '')

@app.route("/NewRecord/addRecord")
def addRecord():
    clientID = request.form.get('clientID', default = 'Error')
    healthID = request.form.get('healthID', default = 'Error')
    dependantID = request.form.get('dependantID', default = 'Error')
    incomeID = request.form.get('incomeID', default = 'Error')
    expenditureID = request.form.get('expenditureID', default = 'Error')
    assetsID = request.form.get('assetsID', default = 'Error')
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("INSERT INTO ClientRecords ('clientID', 'healthID', 'dependantID', 'incomeID', 'expenditureID', 'assetsID')\
                     VALUES (?,?,?,?,?,?)",(clientID, healthID, dependantID, incomeID, expenditureID, assetsID) )
        conn.commit()
        msg = "Relational Record Completed!"
    except Exception as e:
        conn.rollback()
        print(str(e))
        msg = "error in insert operation"
    finally:
        return msg
        conn.close()

#===============================================================================

# DO NOT DELETE REQUIRED FOR SERVER TO RUN! - REQUIRED FOR DEBUG.
if __name__ == "__main__":
	app.run(debug=True)
